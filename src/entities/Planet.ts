type Planet = {
  name: string,
  population: string,
  url: string,
}

export default Planet;
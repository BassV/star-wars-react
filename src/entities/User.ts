type User = {
  name: string,
  gender: string,
  mass: string,
  url: string,
}

export default User;
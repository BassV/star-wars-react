import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { createMuiTheme } from "@material-ui/core/styles";
import { ThemeProvider } from "@material-ui/styles";

import PrivateRoute from "./helpers/PrivateRoute";
import CombinedContext from "./contexts/combinedContext";
import { primaryColor, secondaryColor } from "./constants/colors";
import Home from "./components/Home";
import SignIn from "./components/SignIn";

const materialTheme = createMuiTheme({
  palette: {
    primary: {
      main: primaryColor,
    },
    secondary: {
      main: secondaryColor,
    }
  },
  typography: {
    fontFamily: "'Roboto', sans-serif"
  }
});

const Routes: React.FC = () => {
  return (
    <CombinedContext>
      <ThemeProvider theme={materialTheme}>
        <Router>
          <Switch>
            <Route path="/login" component={SignIn} />
            <PrivateRoute path="/" component={Home} />
          </Switch>
        </Router>
      </ThemeProvider>
    </CombinedContext>
  );
}

export default Routes;

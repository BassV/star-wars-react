import React, { useState, createContext } from "react";
import { useSnackbar } from "notistack";

import SnackbarStatus from "../constants/enums/snackbarStatus";

type SnackbarContextProps = {
  message: string | null | undefined,
  setSnackbarData: (newData: any, status?: SnackbarStatus) => void,
}

type SnackbarContextProviderProps = {
  children?: React.ReactNode
}

export const SnackbarContext = createContext<SnackbarContextProps>({
  message: null,
  setSnackbarData: () => { },
});

const SnackbarContextProvider: React.FC<SnackbarContextProviderProps> = ({ children }) => {
  const [data, setData] = useState<string | null | undefined>();

  const { enqueueSnackbar } = useSnackbar();

  const handleSnackbarDataSet = (message: string, status: SnackbarStatus = SnackbarStatus.error) => {
    enqueueSnackbar(message, { variant: status });
    setData(message);
  };

  return (
    <SnackbarContext.Provider
      value={{ message: data, setSnackbarData: handleSnackbarDataSet }}
    >
      {children}
    </SnackbarContext.Provider>
  );
};

export default SnackbarContextProvider;

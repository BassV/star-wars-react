import React, { useState, createContext } from "react";

import { userLSKey } from "../constants/localStorageKeys";
import User from "../entities/User";

type UserContextProps = {
  localUser: User | null,
  loginUser: (user: User) => void,
  logoutUser: () => void,
}

type UserContextProviderProps = {
  children?: React.ReactNode
}

export const UserContext = createContext<UserContextProps>({
  localUser: null,
  loginUser: () => { },
  logoutUser: () => { },
});

const UserContextProvider: React.FC<UserContextProviderProps> = ({ children }) => {
  const loadLocalStorageUser: any = () => {
    const jsonUser: (string | null) = localStorage.getItem(userLSKey);

    if (jsonUser) {
      return JSON.parse(jsonUser);
    }

    return null;
  };

  const [user, setUser] = useState<(User | null)>(loadLocalStorageUser());

  const handleUserLogin = (user: User) => {
    setUser(user);
    localStorage.setItem(userLSKey, JSON.stringify(user));
  };

  const handleUserLogout = (): void => {
    setUser(null);
    localStorage.removeItem(userLSKey);
  };

  return (
    <UserContext.Provider
      value={{
        localUser: user,
        loginUser: handleUserLogin,
        logoutUser: handleUserLogout,
      }}
    >
      {children}
    </UserContext.Provider>
  );
};

export default UserContextProvider;

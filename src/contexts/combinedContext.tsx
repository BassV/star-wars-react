import React from "react";

import SnackbarContextProvider from "./snackbarContext";
import UserContextProvider from "./userContext";
import { SnackbarProvider } from "../atoms";

type CombinedContextProps = {
  children?: React.ReactNode
}

const CombinedContext: React.FC<CombinedContextProps> = ({ children }) => (
  <SnackbarProvider>
    <SnackbarContextProvider>
      <UserContextProvider>
        {children}
      </UserContextProvider>
    </SnackbarContextProvider>
  </SnackbarProvider>
);

export default CombinedContext;

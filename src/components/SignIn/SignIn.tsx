import React, { useState, useEffect, useContext } from "react";
import { useHistory } from "react-router-dom";
import axios from "axios";

import { Button, Input } from "../../atoms";
import { SnackbarContext } from "../../contexts/snackbarContext";
import { UserContext } from "../../contexts/userContext";
import { userLSKey } from "../../constants/localStorageKeys";
import User from "../../entities/User";

import "./SignIn.scss";

type UsersResult = {
  count: number,
  results: [
    {
      name: string,
      gender: string,
      mass: string,
      url: string,
      birth_year: string,
    }
  ]
}

const SignIn: React.FC = () => {
  const [loading, setLoading] = useState<boolean>(false);
  const [username, setUsername] = useState<string>("");
  const [password, setPassword] = useState<string>("");

  const snackbarContext = useContext(SnackbarContext);
  const userContext = useContext(UserContext);
  const history = useHistory();

  useEffect(() => {
    if (localStorage.getItem(userLSKey)) {
      history.push("/");
    }

    //eslint-disable-next-line
  }, [])

  const handleLoginSubmit = async () => {
    setLoading(true);

    const relativeUrl = "/people";
    const searchQuery = `?search=${username}`;
    const url = process.env.REACT_APP_API_URL + relativeUrl + searchQuery;

    try {
      const headers = { "Access-Control-Allow-Origin": "*" };
      const result = await axios({ method: "GET", url: url, headers: headers });
      const usersResult: UsersResult = result.data;

      if (usersResult.count === 0) {
        snackbarContext.setSnackbarData("Login failed.");
      }

      let loginUser: User | null = null;
      usersResult.results.forEach(user => {
        if (user.name === username && user.birth_year === password) {
          loginUser = {
            name: user.name,
            gender: user.gender,
            mass: user.mass,
            url: user.url
          };
        }
      });

      setLoading(false);

      if (!loginUser) {
        snackbarContext.setSnackbarData("Login failed.");
      } else {
        userContext.loginUser(loginUser);
        history.push("/");
      }
    } catch (error) {
      snackbarContext.setSnackbarData("Login failed.");
      setLoading(false);
    }
  }

  return <div className="SignIn">
    <div className="SignIn__Wrapper">
      <div className="SignIn__Wrapper__Layer"></div>
      <h2 className="SignIn__Wrapper__Title">Sign In</h2>
      <Input
        value={username}
        placeholder="Username"
        marginTop="24px"
        onChange={(event) => setUsername(event.currentTarget.value)}
      />
      <Input
        value={password}
        type="password"
        placeholder="Password"
        marginTop="12px"
        onChange={(event) => setPassword(event.currentTarget.value)}
      />
      <Button label="Login" loading={loading} onClick={handleLoginSubmit} marginTop="72px" />
    </div>
  </div>
}

export default SignIn;
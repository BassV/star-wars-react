import React, { useState, useEffect, useContext } from "react";
import { useHistory } from "react-router-dom";
import { createStyles, fade, Theme, makeStyles } from '@material-ui/core/styles';
import axios from "axios";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import InputBase from "@material-ui/core/InputBase";
import AccountCircle from "@material-ui/icons/AccountCircle";
import SearchIcon from "@material-ui/icons/Search";

import PlanetPreview from "./PlanetPreview/PlanetPreview";
import { UserContext } from "../../contexts/userContext";
import Planet from "../../entities/Planet";

import "./Home.scss";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
    title: {
      flexGrow: 1,
      display: 'none',
      [theme.breakpoints.up('sm')]: {
        display: 'block',
      },
    },
    search: {
      position: 'relative',
      borderRadius: theme.shape.borderRadius,
      backgroundColor: fade(theme.palette.common.white, 0.15),
      '&:hover': {
        backgroundColor: fade(theme.palette.common.white, 0.25),
      },
      marginLeft: 0,
      width: '100%',
      [theme.breakpoints.up('sm')]: {
        marginLeft: theme.spacing(1),
        width: 'auto',
      },
    },
    searchIcon: {
      padding: theme.spacing(0, 2),
      height: '100%',
      position: 'absolute',
      pointerEvents: 'none',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    },
    inputRoot: {
      color: 'inherit',
    },
    inputInput: {
      padding: theme.spacing(1, 1, 1, 0),
      paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
      transition: theme.transitions.create('width'),
      width: '100%',
      [theme.breakpoints.up('sm')]: {
        width: '16ch',
        '&:focus': {
          width: '24ch',
        },
      },
    },
  }),
);

type PlanetsResult = {
  count: number,
  next: string,
  results: [
    planet: Planet
  ]
}

const Home: React.FC = () => {
  const [planets, setPlanets] = useState<Planet[]>([]);
  const [nextUrl, setNextUrl] = useState<string>("");
  const [isBottom, setIsBottom] = useState<boolean>(false);
  const [searchValue, setSearchValue] = useState<string>("");
  const [profileOptionsOpen, setProfileOptionsOpen] = useState<boolean>(false);
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);

  const userContext = useContext(UserContext);
  const classes = useStyles();
  const history = useHistory();

  const getPlanets = async (search: string = "", nextUrl: string = "", isNewSearch: boolean = true) => {
    let url: string = "";

    if (nextUrl) {
      url = nextUrl;
    } else {
      const relativeUrl: string = "/planets";
      const searchQuery: string = `?search=${search}&page=1`;

      url = process.env.REACT_APP_API_URL + relativeUrl + searchQuery;
    }

    try {
      const headers = { "Access-Control-Allow-Origin": "*" };
      const result = await axios({ method: "GET", url: url, headers: headers });
      const planetsResult: PlanetsResult = result.data;

      if (isNewSearch) {
        setPlanets(planetsResult.results);
      } else {
        const newPlanets = planets.concat(planetsResult.results);
        setPlanets(newPlanets);
      }

      setNextUrl(planetsResult.next);
      setIsBottom(false);
    } catch (error) { }
  }

  // Get planets when the page opens
  useEffect(() => {
    const asyncGetPlanets = async () => {
      await getPlanets();
    }

    asyncGetPlanets();

    // eslint-disable-next-line
  }, [])

  // Check if the user is at the bottom of the page
  useEffect(() => {
    const checkLoadMore = () => {
      // Get the top of the body
      const scrollTop = (document.documentElement
        && document.documentElement.scrollTop)
        || document.body.scrollTop;

      // How much the user needs to scroll to get to the bottom
      const scrollHeight = (document.documentElement
        && document.documentElement.scrollHeight)
        || document.body.scrollHeight;

      if (scrollTop + window.innerHeight + 500 >= scrollHeight) {
        setIsBottom(true);
      }
    };

    window.addEventListener("scroll", checkLoadMore);
    return () => {
      window.removeEventListener("scroll", checkLoadMore);
    };
    // eslint-disable-next-line
  }, []);

  // Load more items if the user is at the bottom of the page
  useEffect(() => {
    const asyncGetPlanets = async () => {
      await getPlanets("", nextUrl, false);
    }

    if (isBottom && nextUrl) {
      asyncGetPlanets();
      console.log(`Fetching: ${nextUrl}`);
    }

    // eslint-disable-next-line
  }, [isBottom]);

  // Set timeout for search, to trigger search upon user finishing typing
  useEffect(() => {
    const timeoutId = setTimeout(async () => await getPlanets(searchValue), 500);
    return () => {
      clearTimeout(timeoutId);
    }

    // eslint-disable-next-line
  }, [searchValue])

  const handleProfileIconClick = (event: React.MouseEvent<HTMLElement>): void => {
    setAnchorEl(event.currentTarget);
    setProfileOptionsOpen(true);
  }

  const handleProfileOptionsClose = (): void => {
    setAnchorEl(null);
    setProfileOptionsOpen(false);
  }

  const handleLogoutClick = (): void => {
    userContext.logoutUser();
    history.push("/login");
  }

  const getUsername = (): string => {
    return userContext.localUser ? userContext.localUser.name : "Please Sign In.";
  }

  const handleSearchChange = async (event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>): Promise<void> => {
    const value = event.target.value;
    setSearchValue(value);
  }

  const renderMenu = (
    <Menu
      anchorEl={anchorEl}
      anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
      keepMounted
      transformOrigin={{ vertical: 'top', horizontal: 'right' }}
      open={profileOptionsOpen}
      onClose={handleProfileOptionsClose}
    >
      <MenuItem onClick={handleLogoutClick}>Logout</MenuItem>
    </Menu>
  );

  return <div className="Home">
    <AppBar position="static">
      <Toolbar className="Home__Toolbar">
        <p className="Home__Toolbar__Title">
          {getUsername()}
        </p>
        <div className="Home__Toolbar__Actions">
          <div className={classes.search}>
            <div className={classes.searchIcon}>
              <SearchIcon />
            </div>
            <InputBase
              value={searchValue}
              placeholder="Search planets..."
              onChange={(event => handleSearchChange(event))}
              classes={{
                root: classes.inputRoot,
                input: classes.inputInput,
              }}
              inputProps={{ 'aria-label': 'search' }}
            />
          </div>
          <IconButton
            edge="end"
            aria-label="account of current user"
            aria-haspopup="true"
            onClick={handleProfileIconClick}
            color="inherit"
          >
            <AccountCircle />
          </IconButton>
        </div>
      </Toolbar>
    </AppBar>

    <div className="Home__Planets">
      {planets.length === 0 ? <p className="Home__Planets__Empty">No planet found.</p> :
        planets.map((planet: Planet) => (
          <PlanetPreview key={planet.url} url={planet.url} name={planet.name} population={planet.population} />
        ))
      }
    </div>

    {renderMenu}
  </div>
}

export default Home;
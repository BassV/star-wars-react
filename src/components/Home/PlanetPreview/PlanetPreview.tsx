import React, { useState, useEffect } from "react";

import Planet from "../../../entities/Planet";

import "./PlanetPreview.scss";

const titleFontSize = 20;
const planetSize = 160;
const minPopulation = 0;
const maxPopulation = 1000000000000;
const y1 = 1;
const y2 = 2;

const PlanetPreview: React.FC<Planet> = (planet) => {
  const [multiplier, setMultiplier] = useState<number>(y1);

  const getMultiplier = (): number => {
    const valueNum = getPopulation();

    return y1 + ((Math.log(valueNum + 1) - minPopulation) / (Math.log(maxPopulation) - minPopulation)) * (y2 - y1);
  }

  useEffect(() => {
    setMultiplier(getMultiplier());

    // eslint-disable-next-line
  }, [planet.population])

  const getPopulation = () => {
    let population = parseInt(planet.population);

    if (!population) {
      population = minPopulation;
    }

    return population;
  }

  return <div className="PlanetPreview">
    <div className="PlanetPreview__Texts">
      <p className="PlanetPreview__Texts__Name" style={{ fontSize: titleFontSize * multiplier }}>
        {planet.name}
      </p>
      <p className="PlanetPreview__Texts__Label">Population</p>
      <p className="PlanetPreview__Texts__Value">
        {planet.population}
      </p>
    </div>
    <div className="PlanetPreview__Planet"
      style={{ width: planetSize * multiplier, height: planetSize * multiplier }}
    />
  </div>
}

export default PlanetPreview;
enum SnackbarStatus {
  error = "error",
  warning = "warning",
  info = "info",
  success = "success",
}

export default SnackbarStatus;

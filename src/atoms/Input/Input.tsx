import React from "react";

import "./Input.scss";

type InputProps = {
  value: string,
  placeholder: string,
  onChange: React.ChangeEventHandler<HTMLInputElement>,
  type?: string,
  marginTop?: string,
}

const Input: React.FC<InputProps> = ({ value, placeholder, onChange, type, marginTop }) => {
  return (
    <div className="CustomInput" style={{ marginTop: marginTop }}>
      <input
        className="CustomInput__Input"
        value={value}
        placeholder={placeholder}
        onChange={onChange}
        type={type}
      />
    </div>
  );
};

export default Input;

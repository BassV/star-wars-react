import MUICircularProgress from "@material-ui/core/CircularProgress";

type CircularProgressProps = {
  margin?: string,
  color?: string,
  size?: string,
}

const CircularProgress: React.FC<CircularProgressProps> = ({ margin, color, size = "32px" }) => {
  return <MUICircularProgress
    size={size}
    style={{ margin, color }}
  />
}

export default CircularProgress;
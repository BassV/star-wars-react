import React from "react";
import { SnackbarProvider as MUISnackbarProvider } from "notistack";

import "./SnackbarProvider.scss";

type SnackbarProviderProps = {
  children?: React.ReactNode
}

const SnackbarProvider: React.FC<SnackbarProviderProps> = ({ children }) => {
  return (
    <MUISnackbarProvider
      classes={{
        root: "CustomSnackbarProvider__Root",
        variantError: "CustomSnackbarProvider__Variant",
        variantSuccess: "CustomSnackbarProvider__Variant",
        variantInfo: "CustomSnackbarProvider__Variant",
        variantWarning: "CustomSnackbarProvider__Variant",
      }}
      maxSnack={3}
      hideIconVariant
    >
      {children}
    </MUISnackbarProvider>
  );
};

export default SnackbarProvider;

import { CircularProgress } from "../";

import "./Button.scss";

type ButtonProps = {
  label: string,
  loading: boolean,
  marginTop?: string,
  onClick: React.MouseEventHandler<HTMLButtonElement>,
}

const Button: React.FC<ButtonProps> = ({ label, loading, marginTop, onClick }) => {
  return <button className="CustomButton" onClick={onClick} style={{ marginTop: marginTop }}>
    {loading ? <CircularProgress color="#ffffff" size="26px" /> : label}
  </button>;
};

export default Button;
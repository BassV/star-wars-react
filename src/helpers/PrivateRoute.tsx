import React from "react";
import { Route } from "react-router-dom";

import { userLSKey } from "../constants/localStorageKeys";

type PrivateRouteProps = {
  path: string,
  component: React.FC<any>,
  exact?: boolean,
}

const PrivateRoute: React.FC<PrivateRouteProps> = ({ path, component: Component, exact = false }) => (
  <Route
    path={path}
    exact={exact}
    render={props =>
      localStorage.getItem(userLSKey) ? (
        <Component {...props} />
      ) : (
        (window.location.href = "/login")
      )
    }
  />
);
export default PrivateRoute;
